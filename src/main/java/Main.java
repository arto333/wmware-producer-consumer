import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException {
        Scanner scanner = new Scanner(System.in);
        int N;
        do {
            System.out.println("Insert number of producers 1..10");
            N = Integer.valueOf(scanner.nextLine());
        } while (N < 1 || N > 10);

        int M;
        do {
            System.out.println("Insert number of consumers 1..10");
            M = Integer.valueOf(scanner.nextLine());
        } while (M < 1 || M > 10);

        Lock lock = new ReentrantLock(true);
        Condition queueIsNotEmpty = lock.newCondition();
        Condition queueSizeIs80 = lock.newCondition();
        Queue<Integer> queue = new LinkedList<>();
        AtomicBoolean stopped = new AtomicBoolean(false);
        AtomicBoolean canProduce = new AtomicBoolean(true);
        List<String> consumedData = new LinkedList<>();

        ThreadGroup producerGroup = new ThreadGroup("producers");
        ThreadGroup consumerGroup = new ThreadGroup("consumers");
        Thread[] consumers = new Thread[M];

        for (int i = 0; i < N; ++i) {
            new Thread(producerGroup, new Producer(lock, queueSizeIs80, queueIsNotEmpty, queue, stopped, canProduce)).start();
        }

        for (int i = 0; i < M; ++i) {
            consumers[i] = new Thread(consumerGroup, new Consumer(lock, queueSizeIs80, queueIsNotEmpty, queue, stopped, canProduce, consumedData));
            consumers[i].start();
        }

        Timer queueSizeCounter = new Timer(true);
        queueSizeCounter.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                lock.lock();
                try {
                    System.out.println(queue.size());
                } finally {
                    lock.unlock();
                }
            }
        }, 0, 1000);

        System.out.println("Press Enter to stop program");
        scanner.nextLine();

        lock.lock();
        System.out.println("Stopping program...");
        stopped.set(true);
        // interrupt blocked producers
        producerGroup.interrupt();
        // signal consumers waiting queue to be filled
        queueIsNotEmpty.signalAll();
        lock.unlock();
        queueSizeCounter.cancel();
        // join consumers, to flush consumed data to file
        for (Thread consumer : consumers) {
            consumer.join();
        }
        // cancel timer
        // flush consumed data to file
        Files.write(Paths.get("consumed.txt"), String.join(",", consumedData).getBytes(StandardCharsets.UTF_8));
    }
}