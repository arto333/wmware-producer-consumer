import java.util.Queue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Producer implements Runnable {

    private final Lock queueAccessLock;
    private final Condition queueSizeIs80;
    private final Condition queueIsNotEmpty;
    private final Queue<Integer> queue;
    private final AtomicBoolean stopped;
    private final AtomicBoolean canProduce;

    Producer(final Lock queueAccessLock, final Condition queueSizeIs80, final Condition queueIsNotEmpty, final Queue<Integer> queue, final AtomicBoolean stopped, final AtomicBoolean canProduce) {
        this.queueAccessLock = queueAccessLock;
        this.queueSizeIs80 = queueSizeIs80;
        this.queueIsNotEmpty = queueIsNotEmpty;
        this.queue = queue;
        this.stopped = stopped;
        this.canProduce = canProduce;
    }

    @Override
    public void run() {
        while (!stopped.get()) {
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(0, 101));
            } catch (InterruptedException e) {
                return;
            }

            queueAccessLock.lock();
            try {
                while (!canProduce.get()) {
                    try {
                        queueSizeIs80.await();
                    } catch (InterruptedException e) {
                        return;
                    }
                }

                final int nextInt = ThreadLocalRandom.current().nextInt(0, 101);
                queue.add(nextInt);
                if (queue.size() == 100) {
                    canProduce.set(false);
                }
                queueIsNotEmpty.signalAll();
            } finally {
                queueAccessLock.unlock();
            }
        }
    }
}
