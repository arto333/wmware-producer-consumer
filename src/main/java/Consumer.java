import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class Consumer implements Runnable {

    private final Lock queueAccessLock;
    private final Condition queueSizeIs80;
    private final Condition queueIsNotEmpty;
    private final Queue<Integer> queue;
    private final AtomicBoolean stopped;
    private final AtomicBoolean canProduce;
    private final List<String> consumedData;

    Consumer(final Lock queueAccessLock, final Condition queueSizeIs80, final Condition queueIsNotEmpty, final Queue<Integer> queue, final AtomicBoolean stopped, final AtomicBoolean canProduce, final List<String> consumedData) {
        this.queueAccessLock = queueAccessLock;
        this.queueSizeIs80 = queueSizeIs80;
        this.queueIsNotEmpty = queueIsNotEmpty;
        this.queue = queue;
        this.stopped = stopped;
        this.canProduce = canProduce;
        this.consumedData = consumedData;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(ThreadLocalRandom.current().nextInt(0, 101));
            } catch (InterruptedException e) {
                // Interruptions are not expected for consumers
                e.printStackTrace();
                Thread.currentThread().interrupt();
                throw new IllegalThreadStateException("Interrupted");
            }

            queueAccessLock.lock();
            try {
                while (queue.isEmpty()) {
                    if (stopped.get()) {
                        return;
                    }
                    try {
                        queueIsNotEmpty.await();
                    } catch (InterruptedException e) {
                        // Interruptions are not expected for consumers
                        e.printStackTrace();
                        Thread.currentThread().interrupt();
                        throw new IllegalThreadStateException("Interrupted");
                    }
                }

                consumedData.add(String.valueOf(queue.remove()));
                if (queue.size() == 80) {
                    canProduce.set(true);
                    queueSizeIs80.signalAll();
                }
            } finally {
                queueAccessLock.unlock();
            }
        }
    }
}
